import os
from mango import configs
from typing import List


class Login:
    def __init__(self, username: str, password: str, name: str = "") -> None:
        self.name = name
        self.username = username
        self.password = password

    def __repr__(self) -> str:
        return f"<{self.name} user: {self.username}, password: {self.password}>"

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, Login):
            return (
                self.name == __o.name
                and self.username == __o.username
                and self.password == __o.password
            )
        else:
            return False


def save(login: Login):
    filename = login.username if not login.name else login.name
    _is_available(filename)
    loginfolders = configs["DEFAULT"]["logins_folder"]
    with open(f"{loginfolders}/{filename}.txt", "w") as loginfile:
        loginfile.write(f"{login.name}\n")
        loginfile.write(f"{login.username}\n")
        loginfile.write(f"{login.password}\n")


def _is_available(filename: str) -> None:
    loginfolders = configs["DEFAULT"]["logins_folder"]
    if os.path.isfile(f"{loginfolders}/{filename}.txt"):
        raise ValueError("Login already exists!")


def list() -> List[str]:
    loginsfolder = configs["DEFAULT"]["logins_folder"]
    return os.listdir(loginsfolder)


def parse(login_name: str) -> Login:
    parsed_login = Login("", "", "")
    filepath = f"{configs['DEFAULT']['logins_folder']}/{login_name}.txt"
    with open(filepath, "r") as loginfile:
        parsed_login.name = loginfile.readline().strip()
        parsed_login.username = loginfile.readline().strip()
        parsed_login.password = loginfile.readline().strip()
    return parsed_login
