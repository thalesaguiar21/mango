import os
import configparser
import pathlib

# mangocfg = configparser.ConfigParser()

config_path = pathlib.PosixPath("~/.mango.ini").expanduser()

configs = configparser.ConfigParser()
configs.read(config_path)
if not configs:
    configs = configparser.ConfigParser()
    configs["DEFAULT"] = {
        "config_path": str(config_path),
        "logins_folder": str(pathlib.PosixPath("~/.mango/").expanduser()),
    }

    with open(config_path, "w") as configfile:
        configs.write(configfile)

os.makedirs(configs["DEFAULT"]["logins_folder"], exist_ok=True)
