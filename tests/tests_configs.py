import unittest
import os
import pathlib

from .context import mango
from mango import login


class TestsConfigs(unittest.TestCase):
    def test_no_file_load_defaults(self):
        configpath = pathlib.PosixPath(mango.config_path)
        self.assertTrue(
            os.path.isfile(str(configpath)),
            msg=f"Configuration file not found in {configpath}",
        )

    def test_save(self):
        newlogin = login.Login("dummy", "dummy", "dummy")
        login.save(newlogin)
        logins_folder = mango.configs["DEFAULT"]["logins_folder"]
        self.assertTrue(
            os.path.isfile(f"{logins_folder}/dummy.txt"),
            msg=f"Login File not found {logins_folder}/dummy.txt",
        )


class TestsSave(unittest.TestCase):
    def setUp(self) -> None:
        clear_logins()

    def test_save_complete(self):
        newlogin = login.Login("dummy", "dummy", "dummy")
        login.save(newlogin)
        parsed_login = login.parse("dummy")
        self.assertEqual(parsed_login, newlogin)

    def test_save_no_name(self):
        newlogin = login.Login("dummyuser", "dummy")
        login.save(newlogin)
        parsed_login = login.parse("dummyuser")
        self.assertEqual(parsed_login, newlogin)

    def test_save_override(self):
        firstlogin = login.Login("dummyuser", "dummypass", "dummy")
        login.save(firstlogin)
        with self.assertRaises(ValueError):
            newlogin = login.Login("dummyuser2", "dummypass", "dummy")
            login.save(newlogin)
        parsedlogin = login.parse("dummy")
        self.assertEqual(parsedlogin, firstlogin)

    def test_save_override_no_name_diff_user(self):
        firstlogin = login.Login("dummyuser", "dummypass")
        login.save(firstlogin)
        newlogin = login.Login("dummyuser2", "dummypass")
        login.save(newlogin)
        parsedlogin = login.parse("dummyuser")
        self.assertEqual(parsedlogin, firstlogin)
        parsedlogin = login.parse("dummyuser2")
        self.assertEqual(parsedlogin, newlogin)


class TestsList(unittest.TestCase):
    def setUp(self):
        clear_logins()

    def test_no_logins(self):
        logins = login.list()
        self.assertEqual(logins, [])

    def test_single_login(self):
        newlogin = login.Login("dummy", "dummy", "dummy")
        login.save(newlogin)
        logins = login.list()
        self.assertEqual(logins, ["dummy.txt"])


class TestsParse(unittest.TestCase):
    def setUp(self):
        clear_logins()

    def test_parse_dummy(self):
        newlogin = login.Login("dummy", "dummy", "dummy")
        login.save(newlogin)
        parsedlogin = login.parse(newlogin.name)
        self.assertEqual(newlogin, parsedlogin)


def clear_logins():
    logins_folder = mango.configs["DEFAULT"]["logins_folder"]
    for filepath in os.listdir(logins_folder):
        os.remove(f"{logins_folder}/{filepath}")
